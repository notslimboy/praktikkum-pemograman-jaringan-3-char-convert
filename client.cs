using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    class Program
    {
        const int PORT_NO = 4170;
        const string SERVER_IP = "127.0.0.1";
        static void Main(string[] args)
        {
            TcpClient client = new TcpClient(SERVER_IP, PORT_NO);
            NetworkStream nstream = client.GetStream();
            
            while(true)
            {
                Console.WriteLine("\nKetikkan Huruf: ");
            	string textToSend = Console.ReadLine();
            
            	byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(textToSend);
	            Console.WriteLine("Kirim : " + textToSend);
	            nstream.Write(bytesToSend, 0, bytesToSend.Length);
				
	            byte[] bytesToRead = new byte[client.ReceiveBufferSize];
	            int bytesRead = nstream.Read(bytesToRead, 0, client.ReceiveBufferSize);
	            Console.WriteLine("Terima : " + Encoding.ASCII.GetString(bytesToRead, 0, bytesRead));
            }
            client.Close();
        }
    }
}