
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace server
{
	class Program
    {
        const int PORT_NO = 4170;
        const string SERVER_IP = "127.0.0.1";

        static void Main(string[] args)
        {
            IPAddress localAdd = IPAddress.Parse(SERVER_IP);
            TcpListener listener = new TcpListener(localAdd, PORT_NO);
            listener.Start();

            TcpClient client = listener.AcceptTcpClient();

            NetworkStream nstream = client.GetStream();
            while(true)
            {
            byte[] buffer = new byte[4096];
            int count = nstream.Read(buffer, 0, buffer.Length);

            string receive = Encoding.ASCII.GetString(buffer, 0, count);
            char[] convert = receive.ToCharArray();
            
            for (int i= 0; i<receive.Length; i++)
            {
            	int huruf = convert[i];
            	if(huruf != 32)
                {	
	            	if (huruf >= 65 && huruf <= 90)
                    {
	            		huruf += 32;
}
            		if(huruf >= 119 && huruf <= 122)
                    {
            			huruf -= 26;
            		}
            		huruf += 4;
            	}
            	convert[i] = (char)(huruf);
            }
            
            buffer = Encoding.ASCII.GetBytes(convert);
            nstream.Write(buffer, 0, buffer.Length);
            }
            
            client.Close();
            listener.Stop();
            Console.ReadLine();
        }
    }
}